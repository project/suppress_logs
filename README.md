CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Suppress Logs module provides the ability to suppress logs by log type

 * For the description of the module visit:
   https://www.drupal.org/project/suppress_logs


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Suppress Logs module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module. The system
       breadcrumb block has now been updated.
    2. Navigate to Administration > Configuration > User Interface > Suppress 
       Logs for configurations. Save Configurations.

Configurable options:
 * Types of errors to suppress


MAINTAINERS
-----------

 * Greg Boggs - https://www.drupal.org/u/greg-boggs

Supporting organization:

 * Multnomah County
 * Portland Drupal Users Group
