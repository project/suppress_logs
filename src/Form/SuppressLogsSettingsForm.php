<?php

namespace Drupal\suppress_logs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Build Suppress Logs settings form.
 */
class SuppressLogsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'suppress_logs_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['suppress_logs.settings'];
  }

  /**
   * Builds the form for configuring log types.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The rendered form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('suppress_logs.settings');

    $form['log_types'] = [
    // Changed to textarea for multi-line input.
      '#type' => 'textarea',
      '#title' => $this->t('Target Log Types (line separated)'),
      '#description' => $this->t('Enter one log type per line.'),
      '#default_value' => $config->get('log_types'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $settings = $this->configFactory->getEditable('suppress_logs.settings');

    // Get the values.
    $values = $form_state->cleanValues()->getValues();

    foreach ($values as $field_key => $field_value) {
      $settings->set($field_key, $field_value);
    }
    $settings->save();

    parent::submitForm($form, $form_state);
  }

}
