<?php

namespace Drupal\suppress_logs;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Provides a custom LoggerChannelFactory to suppress logging.
 */
class LoggerChannelFactory implements LoggerChannelFactoryInterface {

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a LoggerChannelFactory object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   */
  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory, ConfigFactoryInterface $configFactory) {
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function get($channel) {
    $config = $this->configFactory->get('suppress_logs.settings');
    if ($config->get('log_types') !== NULL) {
      $logTypes = array_map('trim', explode("\n", $config->get('log_types')));
    }

    if (in_array($channel, $logTypes ?? [])) {
      return new NullLogger();
    }

    return $this->loggerChannelFactory->get($channel);
  }

  /**
   * {@inheritdoc}
   */
  public function addLogger(LoggerInterface $logger, $priority = 0) {
    $this->loggerChannelFactory->addLogger($logger, $priority);
  }

}
